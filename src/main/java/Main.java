import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException {
        Student ns = new Student();
        Method method = ns.getClass().getMethod("doSomething");
        Valid valid = method.getAnnotation(Valid.class);
        System.out.println(valid.name() + " " + valid.phoneNumber());
        ns.doSomething();
    }
}
