public class Student {
    String name;
    int phone;

    @Valid(name = "Nick Scout", phoneNumber = 12345)
    public void doSomething() throws NoSuchMethodException {
        System.out.println(this.getClass().getMethod("doSomething").getAnnotation(Valid.class).name());

    };

    Student() { }
}
